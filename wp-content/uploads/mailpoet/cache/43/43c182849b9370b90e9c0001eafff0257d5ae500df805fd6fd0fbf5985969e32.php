<?php

/* update.html */
class __TwigTemplate_b49f8826e707e85d5786cad5c9a563c32c48cf7eb38a3f3c8fcd8dc2a91b6a77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.html", "update.html", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"wrap mailpoet-about-wrap\">
  <h1>";
        // line 6
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Greetings, humans.");
        echo "</h1>

  <p class=\"about-text\">";
        // line 8
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("The new MailPoet. Simply better. And with regular updates.");
        echo "</p>
  <div class=\"mailpoet-logo\"><img src=\"";
        // line 9
        echo $this->env->getExtension('MailPoet\Twig\Assets')->generateImageUrl("welcome_template/mailpoet-logo.png");
        echo "\" alt=\"MailPoet Logo\" /></div>

  <h2 class=\"nav-tab-wrapper wp-clearfix\">
    <a href=\"admin.php?page=mailpoet-welcome\" class=\"nav-tab\">";
        // line 12
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Welcome");
        echo "</a>
    <a href=\"admin.php?page=mailpoet-update\" class=\"nav-tab nav-tab-active\">";
        // line 13
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("What's New");
        echo "</a>
  </h2>

  <div id=\"mailpoet-changelog\" class=\"feature-section one-col\">
    <h2 class=\"mailpoet-feature-top\">";
        // line 17
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("List of Changes");
        echo "</h2>
    ";
        // line 18
        if (($context["changelog"] ?? null)) {
            // line 19
            echo "      ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["changelog"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 20
                echo "        <h3>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "version", array()), "html", null, true);
                echo "</h3>
        <ul>
          ";
                // line 22
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["item"], "changes", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["change"]) {
                    // line 23
                    echo "            <li>";
                    echo twig_escape_filter($this->env, $context["change"], "html", null, true);
                    echo "</li>
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['change'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 25
                echo "        </ul>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "    ";
        } else {
            // line 28
            echo "      <p style=\"text-align: center\">";
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("See readme.txt for a changelog.");
            echo "</p>
    ";
        }
        // line 30
        echo "    <a class=\"button button-secondary\" href=\"https://wordpress.org/plugins/mailpoet/#developers\" target=\"_blank\">";
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("View all changes");
        echo " &rarr;</a>
  </div>

  <div class=\"feature-section one-col mailpoet_centered\">

    ";
        // line 35
        if (($context["is_new_user"] ?? null)) {
            // line 36
            echo "      <h2>";
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Care to Give Your Opinion?");
            echo "</h2>
      <div class=\"pd-embed\" id=\"pd_1519225570\"></div>
      <script type=\"text/javascript\">
        var _polldaddy = [] || _polldaddy;

        _polldaddy.push( {
          type: \"iframe\",
          auto: \"1\",
          domain: \"mailpoet.polldaddy.com/s/\",
          id: \"improvements-first-steps\",
          placeholder: \"pd_1519225570\"
        } );

        (function(d,c,j){if(!document.getElementById(j)){var pd=d.createElement(c),s;pd.id=j;pd.src=('https:'==document.location.protocol)?'https://polldaddy.com/survey.js':'http://i0.poll.fm/survey.js';s=document.getElementsByTagName(c)[0];s.parentNode.insertBefore(pd,s);}}(document,'script','pd-embed'));
      </script><br>
    ";
        } elseif ((        // line 51
($context["is_old_user"] ?? null) &&  !($context["stop_call_for_rating"] ?? null))) {
            // line 52
            echo "      <h2>";
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Do you enjoy using MailPoet?");
            echo "</h2>
      <p>
        ";
            // line 54
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("If you have the time, we’d really appreciate it if you left us a review on the WordPress Plugin Directory.");
            echo "
        <br>";
            // line 55
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Every star counts.");
            echo "

      <p>
        <a class=\"button\" id=\"no-thanks\">";
            // line 58
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("No thanks");
            echo "</a>
        <a class=\"button\" href=\"admin.php?page=mailpoet-newsletters\">";
            // line 59
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Next time");
            echo "</a>
        <a class=\"button button-primary\" id=\"rate-now\" href=\"https://wordpress.org/support/plugin/mailpoet/reviews/?rate=5#new-post\" target=\"_blank\">";
            // line 60
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Rate now");
            echo "</a>
      <p>
        ";
            // line 62
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Care to share criticism or a feature request instead?");
            echo "
        <br>";
            // line 63
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Click on the question mark (?) in the blue circle at the bottom right of this page to send us a message.");
            echo "
    ";
        } else {
            // line 65
            echo "      <h2>";
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Care to Give Your Opinion?");
            echo "</h2>
      <script type=\"text/javascript\" charset=\"utf-8\" src=\"https://secure.polldaddy.com/p/9957416.js\"></script>
      <noscript><a href=\"https://polldaddy.com/poll/9957416/\">How would you rate the reliability of MailPoet?</a></noscript>
    ";
        }
        // line 69
        echo "
  <hr>

  <div class=\"feature-section one-col mailpoet_centered\">
    <a class=\"button button-primary go-to-plugin\" href=\"admin.php?page=mailpoet-newsletters\">";
        // line 73
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Awesome! Now, take me to MailPoet");
        echo " &rarr;</a>
  </div>

</div>

<script type=\"text/javascript\">
  jQuery(function(\$) {
    function stopShowingCallForRating() {
      return MailPoet.Ajax.post({
        api_version: window.mailpoet_api_version,
        endpoint: 'settings',
        action: 'set',
        data: {
          stop_call_for_rating: true,
        }
      });
    }
    \$('#no-thanks').click(function (e) {
      e.preventDefault();
      stopShowingCallForRating().done(function () {
        window.location.href = 'admin.php?page=mailpoet-newsletters';
      });
      return false;
    });
    \$('#rate-now').click(stopShowingCallForRating);

    \$(function() {
      MailPoet.trackEvent(
        'User has updated MailPoet',
        {'MailPoet Free version': window.mailpoet_version}
      );
    });
    \$('#mailpoet_analytics_enabled').on('click', function() {
      var is_enabled = \$(this).is(':checked') ? true : '';
      MailPoet.Ajax.post({
        api_version: window.mailpoet_api_version,
        endpoint: 'settings',
        action: 'set',
        data: {
          analytics: { enabled: (is_enabled)}
        }
      }).fail(function(response) {
        if (response.errors.length > 0) {
          MailPoet.Notice.error(
            response.errors.map(function(error) { return error.message; }),
            { scroll: true }
          );
        }
      });
    });

  });


</script>
";
    }

    public function getTemplateName()
    {
        return "update.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 73,  185 => 69,  177 => 65,  172 => 63,  168 => 62,  163 => 60,  159 => 59,  155 => 58,  149 => 55,  145 => 54,  139 => 52,  137 => 51,  118 => 36,  116 => 35,  107 => 30,  101 => 28,  98 => 27,  91 => 25,  82 => 23,  78 => 22,  72 => 20,  67 => 19,  65 => 18,  61 => 17,  54 => 13,  50 => 12,  44 => 9,  40 => 8,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "update.html", "/var/www/html/acecompressor/wp-content/plugins/mailpoet/views/update.html");
    }
}
