<?php
/**
 * Custom Functions file for current theme.
 *
 */

// IMPORT PARENT STYLE
function child_theme_enqueue_styles() {
    $parent_style = 'divi-style'; // This is 'divi-style' for the Divi theme.
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    
    wp_enqueue_style( 'custom-design-style',
       get_stylesheet_directory_uri() . '/custom_design.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
	 wp_enqueue_style( 'Bootstrap-style',
       'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'        
    );
	wp_enqueue_style( 'font-awesome-style',
       'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'        
    );
        
        wp_enqueue_style( 'custom-responsive-style',
       get_stylesheet_directory_uri() . '/responsive.css'
    ); 
                
	 wp_enqueue_style( 'owl-style',
       get_stylesheet_directory_uri() . '/owl.carousel.min.css'
    );
         
         wp_enqueue_style( 'flipster-style',
       get_stylesheet_directory_uri() . '/jquery.flipster.min.css'
    ); 
	
         wp_enqueue_script( 'owl-js',
       get_stylesheet_directory_uri() . '/js/owl.carousel.min.js'
    );
         	 wp_enqueue_script( 'ace_custom_front_js',
       get_stylesheet_directory_uri() . '/js/ace_custom.js'        
    );
         	 wp_enqueue_script( 'flipster_js',
       get_stylesheet_directory_uri() . '/js/jquery.flipster.min.js'        
    );
}
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_styles' );

register_nav_menus( array(		
		'social-menu'    => esc_html__( 'Social Menu', 'Divi' ),
		'quick-links'    => esc_html__( 'Quick Links', 'Divi' ),
	) );


// Register  HorizontalSidebar  --------

function ACE_register_sidebars() {
    register_sidebar(
        array(
        'id' => 'horizontal_sidebar',
        'name' => __( 'Horizontal Sidebar in Footer' ),
        'description' => __( 'This is the sidebar to view widget horizontally in the footer.' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3><div class="separator"></div>'
    )
    );
}
add_action( 'widgets_init', 'ACE_register_sidebars' );

/*    shortcode for footer contact details
   */
function ACE_contact_details_in_footer() { 
    ob_start(); 
echo '<div class="wrapper_contact">';
 if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
            <span id="et-info-phone"><?php echo et_sanitize_html_input_text( $et_phone_number ); ?></span>
    <?php endif; ?>

    <?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>
            <a href="<?php echo esc_attr( 'mailto:' . $et_email ); ?>"><span id="et-info-email"><?php echo esc_html( $et_email ); ?></span></a>
    <?php endif;   
    	echo '</div>';
return ob_get_clean();	
 }
add_shortcode('ACE_contact_details_in_footer', 'ACE_contact_details_in_footer');


/*    shortcode for 6 categories on home page
   */
function ACE_categories() {  
    ob_start();
    $parentid = get_queried_object_id();
    $args = array(
        'orderby' => 'name',
        'order' => 'ASC',
        'hide_empty' => false,        
    );

    $terms = get_terms( 'product_cat', $args );

    if ( $terms ) {

        echo '<ul class="product-cats">';

            foreach ( $terms as $term ) {
                if($term->name != 'Uncategorized'){
                echo '<li class="home_category">';                 
					
					echo '<div class="image_wrap_categories">';
                    woocommerce_subcategory_thumbnail( $term );
					echo '</div>';
					
                    echo '<h2>';
                        echo '<a href="' .  esc_url( get_term_link( $term ) ) . '" class="' . $term->slug . '">';
                            echo $term->name;
                        echo '</a>';
                    echo '</h2>';

                echo '</li>';
                } 

        }

        echo '</ul>';
    }
    return ob_get_clean();
}
add_shortcode('ACE_categories', 'ACE_categories');


/* Suppliers Custom Post Type */
function suppliers_init() {
    /* set up Supplier labels */
    $labels = array(
        'name' => 'Suppliers',
        'singular_name' => 'Supplier',
        'add_new' => 'Add New Supplier',
        'add_new_item' => 'Add New Supplier',
        'edit_item' => 'Edit Supplier',
        'new_item' => 'New Supplier',
        'all_items' => 'All Supplier',
        'view_item' => 'View Supplier',
        'search_items' => 'Search Supplier',
        'not_found' =>  'No Suppliers Found',
        'not_found_in_trash' => 'No Suppliers found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Suppliers',
    );
    
    /* register post type */
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'supplier'),
        'query_var' => true,
        'menu_icon' => 'dashicons-randomize',
        'supports' => array(
            'title',   
            'trackbacks',           
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'supplier', $args );
}
add_action( 'init', 'suppliers_init' );


/* Short code for Suppliers Above Footer */

function ACE_suppliers() {  
    ob_start();
?> 
            <div class="parent_carousel_wrapper">
    <h2 class="">Suppliers</h2>
    <div id="supplier_carousel" class="owl-carousel owl-theme">
        <?php 
            $args = array( 'post_type' => 'supplier', 'posts_per_page' => -1, 'order' => 'ASC');
            $the_query = new WP_Query( $args ); 
        ?>
        <?php if ( $the_query->have_posts() ) : 
        while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <div class="img_supplier item">        
             <img src="<?php echo the_post_thumbnail_url();?>" />
        </div>       
        <?php wp_reset_postdata();
        endwhile; else: endif; ?>
    </div></div>
    <?php  return ob_get_clean();
}
add_shortcode('ACE_suppliers', 'ACE_suppliers');


/*
 Shortcode for Offerd
 *  */
function ACE_offers_carousel() {  
    ob_start();
    
   global $woocommerce;  
    $args = array(
        'post_type'      => 'product',
        'posts_per_page' => -1,
        'meta_query'     => array(
            'relation' => 'OR',
            array( // Simple products type
                'key'           => '_sale_price',
                'value'         => 0,
                'compare'       => '>',
                'type'          => 'numeric'
            ),
            array( // Variable products type
                'key'           => '_min_variation_sale_price',
                'value'         => 0,
                'compare'       => '>',
                'type'          => 'numeric'
            )
        )
    );

$sale_product_query = new WP_Query( $args );
?>
<div id="offer_3d">
    <ul class="flip-items">
    <?php
 if ( $sale_product_query->have_posts() ) :
     
    while ( $sale_product_query->have_posts() ) : $sale_product_query->the_post();
     $product_id = get_the_ID(); 
     $part_no_offer_carousel = get_post_meta(get_the_ID(),'_part_no',true);           
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $sale = get_post_meta( get_the_ID(), '_sale_price', true);    
     ?>
    <li>
        <div class="img_offer_product">        
             <img src="<?php echo the_post_thumbnail_url();?>" />
        </div>
        <div class="offer_product_details">
			<span class="sale_wrape"></span>
			<div class="price_wrappe">	
				<p class="title_wrappe"><?php the_title();?></p>
				<p class="cost_wrape"><span>Part No :</span><?php echo $part_no_offer_carousel;?> </p>
				<span class="regular_price"><?php echo $price;?></span>
				<span class="sale_price"><?php echo $sale;?></span>
			</div>
        </div>
    </li> 
    <?php 
    echo '<a class="link_offer_product" href="'.get_permalink($product_id).'"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>';
    endwhile; 
    wp_reset_postdata();
    else: endif; ?>
        </ul>
</div>
 <?php  return ob_get_clean();
}
add_shortcode('ACE_offers_carousel', 'ACE_offers_carousel');


/* Meta Box in Product Post type for Part No */

function ACE_part_no_meta_box()
{
    add_meta_box( 'ACE_part_no_meta_box', 'Product Part Details', 'display_ACE_part_no_meta_box', 'product', 'normal', 'high' );
}

add_action( 'add_meta_boxes', 'ACE_part_no_meta_box' );

function display_ACE_part_no_meta_box(){
	    wp_nonce_field(basename(__FILE__),'nonce_name');
		$value=get_post_meta(get_the_ID(),'_part_no',true);	
					
		?>		
		
		<style type="text/css">	td {width: 200px;}</style>		
		<table>
			<tr>
			
				<td><label for="part_number">Part No: </label></td>
				<td><input type="text" name="part_number" id="part_number" value="<?php echo $value; ?>" /></td>
			</tr>
					
		</table>
<?php	
}

function save_my_meta_value(){
	if(!wp_verify_nonce($_POST['nonce_name'],basename(__FILE__))){
		return;		
	}	
	if(!isset($_POST['part_number'])){
		return;		
	}
	update_post_meta(get_the_ID(),'_part_no',sanitize_text_field($_POST['part_number']));	
}

add_action('save_post','save_my_meta_value');

/* 
 *  Divi WordPress Theme and WooCommerce plugin
 *  Make the Add To Cart buttons appear on the WooCommerce shop page. 
 */
 
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 20 );




/**
 * Add quantity field on the archive page.
 */
function custom_quantity_field_archive() {
	$product = wc_get_product( get_the_ID() );
	if ( ! $product->is_sold_individually() && 'variable' != $product->product_type && $product->is_purchasable() ) {
		woocommerce_quantity_input( array( 'min_value' => 1, 'max_value' => $product->backorders_allowed() ? '' : $product->get_stock_quantity() ) );
	}
}
add_action( 'woocommerce_after_shop_loop_item', 'custom_quantity_field_archive', 0, 9 );
/**
 * Add requires JavaScript.
 */
function custom_add_to_cart_quantity_handler() {
	wc_enqueue_js( '
		jQuery( ".post-type-archive-product" ).on( "click", ".quantity input", function() {
			return false;
		});
		jQuery( ".post-type-archive-product, .archive" ).on( "change input", ".quantity .qty", function() {
			var add_to_cart_button = jQuery( this ).parents( ".product" ).find( ".add_to_cart_button" );
			// For AJAX add-to-cart actions
			add_to_cart_button.data( "quantity", jQuery( this ).val() );
			// For non-AJAX add-to-cart actions
			add_to_cart_button.attr( "href", "?add-to-cart=" + add_to_cart_button.attr( "data-product_id" ) + "&quantity=" + jQuery( this ).val() );
		});
	' );
}
add_action( 'init', 'custom_add_to_cart_quantity_handler' );