<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<style>
ul.newulmenu {
    padding: 0px !important;
}
li.newlimenu {
    list-style-type: none;
    float: left;
    padding-right: 10px;
    font-size: 14px;
}
</style>
<form class="woocommerce-ordering" method="get">
	<select name="orderby" class="orderby newddlorder" style="display:none;">
		<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
			<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
		<?php endforeach; ?>
	</select>
    <ul class="newulmenu">
        <?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
        <li class="newlimenu"><a class="custom_sorting" href="#" data-value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( str_replace( "Sort by", "", $name ) ); ?></a></li>
        <?php endforeach; ?>
    </ul> 
        <input type="hidden" name="post_type" value="product" />
	<input type="hidden" name="paged" value="1" />
	<?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>
</form>
