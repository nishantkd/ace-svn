<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>
        
 <?php 
        if( ! is_front_page() ){
            echo do_shortcode('[ACE_services_footer]');
        }
        echo do_shortcode('[ACE_suppliers]'); ?>
			<footer id="main-footer">
				<?php get_sidebar( 'footer' ); ?>
                                
		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #et-footer-nav -->

			<?php endif; ?>
                                <div class="horizontal_footer">
                                    <div class="horizontal-sidebar">                    
                                        <?php dynamic_sidebar( 'horizontal_sidebar' ); ?>
                                    </div> 
                                </div>

				<div id="footer-bottom">
                                    <?php 
                                    $copyright_text = get_theme_mod( 'footer_copyright_textbox' );
                                    $developed_by = get_theme_mod( 'footer_designedby_textbox' );
                                    ?>
					<div class="container clearfix">
                                          <span class="bottom_footer_left">  <?php echo $copyright_text;?></span>
                                            <span class="bottom_footer_right"><?php echo $developed_by;?></span>
                                            
				<?php 
					/*if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
						get_template_part( 'includes/social_icons', 'footer' );
					}

					echo et_get_footer_credits();*/
				?>
					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
</body>
<script>
    var carousel = jQuery("#offer_3d").flipster({
        style: 'flat',
        spacing: -0.5,
        nav: true,
        buttons:   true
    });
</script>
</html>