<?php
/**
 * Custom Functions file for current theme.
 *
 */

// IMPORT PARENT STYLE
function child_theme_enqueue_styles() {
    $parent_style = 'divi-style'; // This is 'divi-style' for the Divi theme.
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    
    wp_enqueue_style( 'custom-design-style',
       get_stylesheet_directory_uri() . '/custom_design.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
	 wp_enqueue_style( 'Bootstrap-style',
       'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'        
    );
	wp_enqueue_style( 'font-awesome-style',
       'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'        
    );
        
        wp_enqueue_style( 'custom-responsive-style',
       get_stylesheet_directory_uri() . '/responsive.css'
    ); 
                
	 wp_enqueue_style( 'owl-style',
       get_stylesheet_directory_uri() . '/owl.carousel.min.css'
    );
         
         wp_enqueue_style( 'flipster-style',
       get_stylesheet_directory_uri() . '/jquery.flipster.min.css'
    ); 
	
         wp_enqueue_script( 'owl-js',
       get_stylesheet_directory_uri() . '/js/owl.carousel.min.js'
    );
         	 wp_enqueue_script( 'ace_custom_front_js',
       get_stylesheet_directory_uri() . '/js/ace_custom.js'        
    );
         	 wp_enqueue_script( 'flipster_js',
       get_stylesheet_directory_uri() . '/js/jquery.flipster.min.js'        
    );
}
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_styles' );

register_nav_menus( array(		
		'social-menu'    => esc_html__( 'Social Menu', 'Divi' ),
		'quick-links'    => esc_html__( 'Quick Links', 'Divi' ),
	) );


// Register  HorizontalSidebar  --------

function ACE_register_sidebars() {
    register_sidebar(
        array(
        'id' => 'horizontal_sidebar',
        'name' => __( 'Horizontal Sidebar in Footer' ),
        'description' => __( 'This is the sidebar to view widget horizontally in the footer.' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3><div class="separator"></div>'
    )
    );
}
add_action( 'widgets_init', 'ACE_register_sidebars' );

/*    shortcode for footer contact details
   */
function ACE_contact_details_in_footer() { 
    ob_start(); 
echo '<div class="wrapper_contact">';
 if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
            <span id="et-info-phone"><?php echo et_sanitize_html_input_text( $et_phone_number ); ?></span>
    <?php endif; ?>

    <?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>
            <a href="<?php echo esc_attr( 'mailto:' . $et_email ); ?>"><span id="et-info-email"><?php echo esc_html( $et_email ); ?></span></a>
    <?php endif;   
    	echo '</div>';
return ob_get_clean();	
 }
add_shortcode('ACE_contact_details_in_footer', 'ACE_contact_details_in_footer');


/*    shortcode for 6 categories on home page
   */
function ACE_categories() {  
    ob_start();
    $parentid = get_queried_object_id();
    $args = array(
        'orderby' => 'name',
        'order' => 'ASC',
        'hide_empty' => false,        
    );

    $terms = get_terms( 'product_cat', $args );

    if ( $terms ) {

        echo '<ul class="product-cats">';

            foreach ( $terms as $term ) {
                if($term->name != 'Uncategorized'){
                echo '<li class="home_category">';                 
					
					echo '<div class="image_wrap_categories">';
                    woocommerce_subcategory_thumbnail( $term );
					echo '</div>';
					
                    echo '<h2>';
                        echo '<a href="' .  esc_url( get_term_link( $term ) ) . '" class="' . $term->slug . '">';
                            echo $term->name;
                        echo '</a>';
                    echo '</h2>';

                echo '</li>';
                } 

        }

        echo '</ul>';
    }
    return ob_get_clean();
}
add_shortcode('ACE_categories', 'ACE_categories');


/* Suppliers Custom Post Type */
function suppliers_init() {
    /* set up Supplier labels */
    $labels = array(
        'name' => 'Suppliers',
        'singular_name' => 'Supplier',
        'add_new' => 'Add New Supplier',
        'add_new_item' => 'Add New Supplier',
        'edit_item' => 'Edit Supplier',
        'new_item' => 'New Supplier',
        'all_items' => 'All Supplier',
        'view_item' => 'View Supplier',
        'search_items' => 'Search Supplier',
        'not_found' =>  'No Suppliers Found',
        'not_found_in_trash' => 'No Suppliers found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Suppliers',
    );
    
    /* register post type */
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'supplier'),
        'query_var' => true,
        'menu_icon' => 'dashicons-randomize',
        'supports' => array(
            'title',   
            'trackbacks',           
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'supplier', $args );
}
add_action( 'init', 'suppliers_init' );


/* Short code for Suppliers Above Footer */

function ACE_suppliers() {  
    ob_start();
?> 
            <div class="parent_carousel_wrapper">
    <h2 class="">Suppliers</h2>
    <div id="supplier_carousel" class="owl-carousel owl-theme">
        <?php 
            $args = array( 'post_type' => 'supplier', 'posts_per_page' => -1, 'order' => 'ASC');
            $the_query = new WP_Query( $args ); 
        ?>
        <?php if ( $the_query->have_posts() ) : 
        while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <div class="img_supplier item">        
             <img src="<?php echo the_post_thumbnail_url();?>" />
        </div>       
        <?php wp_reset_postdata();
        endwhile; else: endif; ?>
    </div></div>
    <?php  return ob_get_clean();
}
add_shortcode('ACE_suppliers', 'ACE_suppliers');


/*
 Shortcode for Offerd
 *  */
function ACE_offers_carousel() {  
    ob_start();
    
   global $woocommerce;  
    $args = array(
        'post_type'      => 'product',
        'posts_per_page' => -1,
        'meta_query'     => array(
            'relation' => 'OR',
            array( // Simple products type
                'key'           => '_sale_price',
                'value'         => 0,
                'compare'       => '>',
                'type'          => 'numeric'
            ),
            array( // Variable products type
                'key'           => '_min_variation_sale_price',
                'value'         => 0,
                'compare'       => '>',
                'type'          => 'numeric'
            )
        )
    );

$sale_product_query = new WP_Query( $args );
?>
<div id="offer_3d">
    <ul class="flip-items">
    <?php
 if ( $sale_product_query->have_posts() ) :
     
    while ( $sale_product_query->have_posts() ) : $sale_product_query->the_post();
     $product_id = get_the_ID(); 
     $part_no_offer_carousel = get_post_meta(get_the_ID(),'_part_no',true);           
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $sale = get_post_meta( get_the_ID(), '_sale_price', true);    
     ?>
    <li>
        <div class="img_offer_product">        
             <img src="<?php echo the_post_thumbnail_url();?>" />
        </div>
        <div class="offer_product_details">
			<span class="sale_wrape"></span>
			<div class="price_wrappe">	
				<p class="title_wrappe"><?php the_title();?></p>
				<p class="cost_wrape"><span>Part No :</span><?php echo $part_no_offer_carousel;?> </p>
				<span class="regular_price"><?php echo $price;?></span>
				<span class="sale_price"><?php echo $sale;?></span>
				<?php 
				echo '<a class="link_offer_product" href="'.get_permalink($product_id).'"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>';
				endwhile; 
				wp_reset_postdata();
				else: endif; ?>
			</div>
			
        </div>
    </li> 
    
        </ul>
</div>
 <?php  return ob_get_clean();
}
add_shortcode('ACE_offers_carousel', 'ACE_offers_carousel');


/* Meta Box in Product Post type for Part No */

function ACE_meta_box_product()
{
    add_meta_box( 'ACE_meta_box_product', 'Product Part Details', 'display_ACE_meta_box_product', 'product', 'normal', 'high' );
}

add_action( 'add_meta_boxes', 'ACE_meta_box_product' );

function display_ACE_meta_box_product(){
	    wp_nonce_field(basename(__FILE__),'nonce_name');
		$value=get_post_meta(get_the_ID(),'_part_no',true);
                
		$ace_price=get_post_meta(get_the_ID(),'_ace_price',true);	
					
		?>		
		
		<style type="text/css">	td {width: 200px;}</style>		
		<table>
			<tr>			
				<td><label for="part_number">Part No: </label></td>
				<td><input type="text" name="part_number" id="part_number" value="<?php echo $value; ?>" /></td>
			</tr>
			<tr>			
				<td><label for="ace_price">ACE Price: </label></td>
				<td><input type="text" name="ace_price" id="ace_price" value="<?php echo $ace_price; ?>" /></td>
			</tr>
					
		</table>
<?php	
}

function save_my_meta_value(){
	if(!wp_verify_nonce($_POST['nonce_name'],basename(__FILE__))){
		return;		
	}	
	if(!isset($_POST['part_number'])){
		return;		
	}
	if(!isset($_POST['ace_price'])){
		return;		
	}
	update_post_meta(get_the_ID(),'_part_no',sanitize_text_field($_POST['part_number']));	
	update_post_meta(get_the_ID(),'_ace_price',sanitize_text_field($_POST['ace_price']));	
}

add_action('save_post','save_my_meta_value');

/* 
 *  Divi WordPress Theme and WooCommerce plugin
 *  Make the Add To Cart buttons appear on the WooCommerce shop page. 
 */
 
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 20 );




/**
 * Add quantity field on the archive page.
 */
function custom_quantity_field_archive() {
	$product = wc_get_product( get_the_ID() );
	if ( ! $product->is_sold_individually() && 'variable' != $product->product_type && $product->is_purchasable() ) {
		woocommerce_quantity_input( array( 'min_value' => 1, 'max_value' => $product->backorders_allowed() ? '' : $product->get_stock_quantity() ) );
	}
}
add_action( 'woocommerce_after_shop_loop_item', 'custom_quantity_field_archive', 12 );


/* show product part no on archive page */

function ACE_part_no_func(){        
    $product_part_no = get_post_meta(get_the_ID(),'_part_no',true);
    $product_ace_price = get_post_meta(get_the_ID(),'_ace_price',true);
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $sale = get_post_meta( get_the_ID(), '_sale_price', true);    
    ?>
    <p class="product_part_no"><span>Part No : </span><?php echo $product_part_no;?> </p>    
    <p class="product_price_archive"><span>RRP : </span><?php echo $price;?> </p>
    <p class="product_sale_archive"><span>WAS : </span><?php echo $sale;?> </p>
    <div class="price_wrappe"><p class="product_ace_price"><span>ACE Price : </span></br><?php echo $product_ace_price;?> </p></div>
    <?php
}
add_action('woocommerce_shop_loop_item_title','ACE_part_no_func',12);

/**
 * Add requires JavaScript.
 */
function custom_add_to_cart_quantity_handler() {
	wc_enqueue_js( '
		jQuery( ".post-type-archive-product" ).on( "click", ".quantity input", function() {
			return false;
		});
		jQuery( ".post-type-archive-product, .archive" ).on( "change input", ".quantity .qty", function(event) {
                        event.preventDefault();
			var add_to_cart_button = jQuery( this ).parents( ".product" ).find( ".add_to_cart_button" );
                        
                        // For AJAX add-to-cart actions
			add_to_cart_button.data( "quantity", jQuery( this ).val() );
			
                        // For non-AJAX add-to-cart actions
			add_to_cart_button.attr( "href", "?add-to-cart=" + add_to_cart_button.attr( "data-product_id" ) + "&quantity=" + jQuery( this ).val() );
                        
		});
                
                jQuery(document).on("click", "a.custom_sorting", function (event) {                   
                            event.preventDefault();
                            var value_a =jQuery(this).data("value");                            
                            jQuery(".newddlorder").val(value_a);
                            jQuery(".newddlorder").trigger("change");
                        });
	' );
}
add_action( 'init', 'custom_add_to_cart_quantity_handler' );

/**
 * Register Panel in Customizer.
 */

function ACE_childheme_customize_register( $wp_customize ) {
    
    //Adding a section
    $wp_customize->add_section(
                        'footer_setting_section',
                        array(
                            'title' => 'Footer Bottombar Settings',
                            'description' => 'This section club the setting for the bottombar in fotter of the theme.',
                            'priority' => 9999,
                        )
    );
    
    //Add a setting
    $wp_customize->add_setting(
                    'footer_designedby_textbox',
                    array(
                         'default' => 'Designed by & Developed by',
                    )
    );
    
    //Add control
     $wp_customize->add_control(
                    'footer_designedby_textbox',
                    array(
                        'label' => 'Designed By text',
                        'section' => 'footer_setting_section',
                        'type' => 'text',
                    )
    );
    //Add a setting
    $wp_customize->add_setting(
                    'footer_copyright_textbox',
                    array(
                         'default' => 'Copyright Text',
                    )
    );
    
    //Add control
     $wp_customize->add_control(
                    'footer_copyright_textbox',
                    array(
                        'label' => 'Copyright text',
                        'section' => 'footer_setting_section',
                        'type' => 'text',
                    )
    );
}

add_action( 'customize_register', 'ACE_childheme_customize_register' );


/**
 * News Post type registration
 */

function product_init() {
    // set up News labels
    $labels = array(
        'name' => 'News',
        'singular_name' => 'News',
        'add_new' => 'Add New News',
        'add_new_item' => 'Add New News',
        'edit_item' => 'Edit News',
        'new_item' => 'New News',
        'all_items' => 'All News',
        'view_item' => 'View News',
        'search_items' => 'Search News',
        'not_found' =>  'No News Found',
        'not_found_in_trash' => 'No News found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'News',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'news'),
        'query_var' => true,
        'menu_icon' => 'dashicons-randomize',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'news', $args );
    
}
add_action( 'init', 'product_init' );


/* 
 *  Shortcode for news section from CPT news
 *  
 */
function ACE_news() { 
    ob_start(); 
    
            $args = array( 'post_type' => 'news', 'posts_per_page' => 3, 'order' => 'DESC');
            $the_query = new WP_Query( $args ); 
            $news_id = get_the_ID();
        ?>
                <div id="news_section_home" class="et_pb_blog_grid clearfix et_pb_module et_pb_bg_layout_light  et_pb_blog_0">
        <?php if ( $the_query->have_posts() ) : 
            
        while ( $the_query->have_posts() ) : $the_query->the_post(); ?>               
        <div class="column size-1of3">
                <article id="post-<?php echo $news_id;?>" class="et_pb_post clearfix post-<?php echo $news_id;?> post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized">
            <div class="et_pb_image_container">
                <a href="<?php echo get_post_permalink(); ?>" class="entry-featured-image-url">
                    <img src="<?php echo the_post_thumbnail_url();?>" alt='' width='400' height='250' /></a>
            </div> <!-- .et_pb_image_container -->
            <h2 class="entry-title">
                <a href="<?php echo get_post_permalink(); ?>"><?php the_title();?></a>
            </h2>				
            <p class="post-meta"> 
                <span class="published"><?php echo get_the_date('F j, Y'); ?></span>  
            </p>
            <div class="post-content">
                <p>
                    <?php
                        $content = get_the_content();
                        $trimmed_content = wp_trim_words( $content, 25, NULL );
                        echo $trimmed_content;
                    ?>
                </p>
                <a href="<?php echo get_post_permalink(); ?>" class="more-link" >read</a>
            </div>			
        </article> <!-- .et_pb_post -->     
              </div><!--.column-->
        <?php wp_reset_postdata();
        endwhile; else: endif; 
 ?>
     </div><!--#news_section_home-->
     <?php
return ob_get_clean();	
 }
add_shortcode('ACE_news', 'ACE_news');

/*
* Services post type
*/

function ACE_services_init() {
    // set up product labels
    $labels = array(
        'name' => 'Services',
        'singular_name' => 'Service',
        'add_new' => 'Add New Service',
        'add_new_item' => 'Add New Service',
        'edit_item' => 'Edit Service',
        'new_item' => 'New Service',
        'all_items' => 'All Services',
        'view_item' => 'View Service',
        'search_items' => 'Search Services',
        'not_found' =>  'No Service Found',
        'not_found_in_trash' => 'No Service found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Service',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'service'),
        'query_var' => true,
        'menu_icon' => 'dashicons-randomize',
        'supports' => array(
            'title',
            'editor',            
            'trackbacks',            
            'revisions', 
        )
    );
    register_post_type( 'service', $args );
    
    
}
add_action( 'init', 'ACE_services_init' );

/* 
 *  Shortcode for news section from CPT news
 *  
 */
function ACE_services_footer() { 
    ob_start(); 
    
            $args = array( 'post_type' => 'service', 'posts_per_page' => 4, 'order' => 'DESC');
            $the_query = new WP_Query( $args ); 
            $news_id = get_the_ID();
        ?>
        <div id="banner_four_blocks" class="et_animated et_pb_row et_pb_row_0 et_pb_row_fullwidth et_pb_row_4col slideBottom" style="animation-duration: 1000ms; animation-delay: 0ms; opacity: 0; animation-timing-function: ease-in-out; transform: translate3d(0px, 100%, 0px);">
        <?php if ( $the_query->have_posts() ) : 
            $count = 0;
        while ( $the_query->have_posts() ) : $the_query->the_post(); ?>               
        <div class="et_pb_column et_pb_column_1_4  et_pb_column_<?php echo $count;?> et_pb_css_mix_blend_mode_passthrough">			
            <div class="et_pb_blurb et_pb_module et_pb_bg_layout_dark et_pb_text_align_center  et_pb_blurb_2 et_pb_blurb_position_top">
            <div class="et_pb_blurb_content">					
                    <div class="et_pb_blurb_container">
                            <h4 class="et_pb_module_header"><?php the_title();?></h4>
                            <div class="et_pb_blurb_description">
                                    <p><?php the_content();?></p>
                            </div><!-- .et_pb_blurb_description -->
                    </div>
            </div> <!-- .et_pb_blurb_content -->
        </div> <!-- .et_pb_blurb -->
        </div>
        <?php
        $count++;
        wp_reset_postdata();
        endwhile; else: endif; 
 ?>
     </div><!--#banner_four_blocks-->
     <?php
return ob_get_clean();	
 }
add_shortcode('ACE_services_footer', 'ACE_services_footer');


/* Category custom paragraph above and below */

//Product Cat Create page
function ACE_taxonomy_add_new_meta_field() {
    ?>

    <div class="form-field">
        <label for="ace_top_meta_discription"><?php _e('Enter Top Description', 'ACE'); ?></label>
       <!-- <input type="text" name="ace_top_meta_discription" id="ace_top_meta_discription">-->
         <textarea name="ace_top_meta_discription" id="ace_top_meta_discription"></textarea>
        <p class="description"><?php _e('Enter Top Description, <= 60 character', 'ACE'); ?></p>
    </div>
    <div class="form-field">
        <label for="ace_bottom_meta_discription"><?php _e('Enter Bottom Description', 'ACE'); ?></label>
        <textarea name="ace_bottom_meta_discription" id="ace_bottom_meta_discription"></textarea>
        <p class="description"><?php _e('Enter Bottom Description, <= 160 character', 'ACE'); ?></p>
    </div>
    <?php
}

//Product Cat Edit page
function ACE_taxonomy_edit_meta_field($term) {

    //getting term ID
    $term_id = $term->term_id;

    // retrieve the existing value(s) for this meta field.
    $ace_top_meta_discription = get_term_meta($term_id, 'ace_top_meta_discription', true);
    $ace_bottom_meta_discription = get_term_meta($term_id, 'ace_bottom_meta_discription', true);
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="ace_top_meta_discription"><?php _e('Enter Top Description', 'ACE'); ?></label></th>        
        <td>
            <textarea name="ace_top_meta_discription" id="ace_top_meta_discription"><?php echo esc_attr($ace_top_meta_discription) ? esc_attr($ace_top_meta_discription) : ''; ?></textarea>
            <p class="description"><?php _e('Enter a top paragraph', 'ACE'); ?></p>
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="ace_bottom_meta_discription"><?php _e('Enter Bottom paragraph', 'ACE'); ?></label></th>
        <td>
            <textarea name="ace_bottom_meta_discription" id="ace_bottom_meta_discription"><?php echo esc_attr($ace_bottom_meta_discription) ? esc_attr($ace_bottom_meta_discription) : ''; ?></textarea>
            <p class="description"><?php _e('Enter a bottom paragraph', 'ACE'); ?></p>
        </td>
    </tr>
    <?php
}

add_action('product_cat_add_form_fields', 'ACE_taxonomy_add_new_meta_field', 10, 1);
add_action('product_cat_edit_form_fields', 'ACE_taxonomy_edit_meta_field', 10, 1);

// Save extra taxonomy fields callback function.
function ACE_save_taxonomy_custom_meta($term_id) {

    $ace_top_meta_discription = filter_input(INPUT_POST, 'ace_top_meta_discription');
    $ace_bottom_meta_discription = filter_input(INPUT_POST, 'ace_bottom_meta_discription');

    update_term_meta($term_id, 'ace_top_meta_discription', $ace_top_meta_discription);
    update_term_meta($term_id, 'ace_bottom_meta_discription', $ace_bottom_meta_discription);
}

add_action('edited_product_cat', 'ACE_save_taxonomy_custom_meta', 10, 1);
add_action('create_product_cat', 'ACE_save_taxonomy_custom_meta', 10, 1);


/* Breadcrumb Outside container changes */
/*Reposition WooCommerce breadcrumb */
function woocommerce_remove_breadcrumb(){
remove_action( 
    'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
}
add_action(
    'woocommerce_before_main_content', 'woocommerce_remove_breadcrumb'
);

function woocommerce_custom_breadcrumb(){
    woocommerce_breadcrumb();
}

add_action( 'woo_custom_breadcrumb', 'woocommerce_custom_breadcrumb' );