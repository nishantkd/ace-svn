<?php get_header();
do_action('woo_custom_breadcrumb'); 	
 ?>

<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
		<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					$post_format = et_pb_post_format(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>

				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_pb_post_main_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					et_divi_post_format_content();

					if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) {
						if ( 'video' === $post_format && false !== ( $first_video = et_get_first_video() ) ) :
							printf(
								'<div class="et_main_video_container">
									%1$s
								</div>',
								$first_video
							);
						elseif ( ! in_array( $post_format, array( 'gallery' ) ) && 'on' === et_get_option( 'divi_thumbnails_index', 'on' ) && '' !== $thumb ) : ?>
							<div class="left_blog_main_img"><a href="<?php the_permalink(); ?>">
								<?php print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height ); ?>
							</a></div>
					<?php
						elseif ( 'gallery' === $post_format ) :
							et_pb_gallery_images();
						endif;
					} ?>

				<?php if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) : ?>
					<?php if ( ! in_array( $post_format, array( 'link', 'audio' ) ) ) : ?>
					<div class="blog_right_content_wrap"><div class="inner_wrappe_main_div"><h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<?php endif; ?>
                                                <p class="date_wrappe_main_div"><a href="javascript:void(0);" class="date_single_post"><?php the_date('d F, Y'); ?></a></p>
					<?php						
                                                $id = get_the_id();
                                                $terms = get_the_terms( $id, 'category' );
                                                 $term_id = $terms[0]->term_id;
                                                ?>                                               
                                                <span class="cat_name_title">Categories: </span>    
                                                <?php
                                                foreach($terms as $term) {
                                                    ?><a href="<?php echo home_url();?>/?cat=<?php echo $term_id; ?>" class="cat_name_single"><?php echo $term->name; ?></a><?php
                                                }
                                                echo "</div>";

						if ( 'on' !== et_get_option( 'divi_blog_style', 'false' ) || ( is_search() && ( 'on' === get_post_meta( get_the_ID(), '_et_pb_use_builder', true ) ) ) ) {
							 ?>
							 <p class="the_content_wrappe">
							<?php echo ''.truncate_post( 270 ).''; ?>
							</p>
						<?php } else {
								
							the_content();
						}
					?>
                                       <a class='reasmore' href='<?php the_permalink(); ?>'>Read more</a>  </div>
									   <div class="clear"></div>
				<?php endif; ?>

					</article> <!-- .et_pb_post -->
			<?php
					endwhile;
                                        
                                         if ( function_exists( 'wp_pagination' ) ):
                                               wp_pagination();
                                        endif;
				
				else :
					get_template_part( 'includes/no-results', 'index' );
				endif;
			?>
			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->
<div class="featured_on_blogpage">
<?php echo do_shortcode('[showmodule id="298"]');?>
</div>
<?php get_footer(); ?>